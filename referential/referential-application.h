/*
 * referential-application.h: gobject watch application
 *
 * Copyright (C) 2011 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * Author: Daniel P. Berrange <berrange@redhat.com>
 */

#ifndef __REFERENTIAL_APPLICATION_H__
#define __REFERENTIAL_APPLICATION_H__

#include <glib-object.h>

#include "referential/referential-session.h"

G_BEGIN_DECLS

#define REFERENTIAL_TYPE_APPLICATION            (referential_application_get_type ())
#define REFERENTIAL_APPLICATION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), REFERENTIAL_TYPE_APPLICATION, ReferentialApplication))
#define REFERENTIAL_APPLICATION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), REFERENTIAL_TYPE_APPLICATION, ReferentialApplicationClass))
#define REFERENTIAL_IS_APPLICATION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), REFERENTIAL_TYPE_APPLICATION))
#define REFERENTIAL_IS_APPLICATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), REFERENTIAL_TYPE_APPLICATION))
#define REFERENTIAL_APPLICATION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), REFERENTIAL_TYPE_APPLICATION, ReferentialApplicationClass))


typedef struct _ReferentialApplication ReferentialApplication;
typedef struct _ReferentialApplicationPrivate ReferentialApplicationPrivate;
typedef struct _ReferentialApplicationClass ReferentialApplicationClass;

struct _ReferentialApplication
{
    GObject parent;

    ReferentialApplicationPrivate *priv;

    /* Do not add fields to this struct */
};

struct _ReferentialApplicationClass
{
    GObjectClass parent_class;

    gpointer padding[20];
};

GType referential_application_get_type(void);

ReferentialApplication *referential_application_new(const gchar *command,
						    const gchar *parasite,
						    const gchar *filter);

gboolean referential_application_run(ReferentialApplication *app,
				     GError **err);

ReferentialSession *referential_application_snapshot_session(ReferentialApplication *app);

const gchar *referential_application_get_command(ReferentialApplication *app);
const gchar *referential_application_get_parasite(ReferentialApplication *app);
const gchar *referential_application_get_filter(ReferentialApplication *app);

G_END_DECLS

#endif /* __REFERENTIAL_APPLICATION_H__ */
