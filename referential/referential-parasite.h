/*
 * referential-parasite.h: helper APIs for sending data from a parasite
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

#ifndef __REFERENTIAL_PARASITE_H__
#define __REFERENTIAL_PARASITE_H__

void referential_init(void);
void referential_report_new(void *objaddr,
			    const char *typename,
			    int refs);
void referential_report_ref(void *objaddr,
			    int refs);
void referential_report_unref(void *objaddr,
			      int refs);
void referential_report_free(void *objaddr);


#endif /* __REFERENTIAL_PARASITE_H__ */
