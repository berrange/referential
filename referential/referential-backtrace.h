/*
 * referential-backtrace.h: gobject watch backtrace
 *
 * Copyright (C) 2011 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * Author: Daniel P. Berrange <berrange@redhat.com>
 */

#ifndef __REFERENTIAL_BACKTRACE_H__
#define __REFERENTIAL_BACKTRACE_H__

#include <glib-object.h>

#include "referential/referential-symbol.h"

G_BEGIN_DECLS

#define REFERENTIAL_TYPE_BACKTRACE            (referential_backtrace_get_type ())
#define REFERENTIAL_BACKTRACE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), REFERENTIAL_TYPE_BACKTRACE, ReferentialBacktrace))
#define REFERENTIAL_BACKTRACE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), REFERENTIAL_TYPE_BACKTRACE, ReferentialBacktraceClass))
#define REFERENTIAL_IS_BACKTRACE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), REFERENTIAL_TYPE_BACKTRACE))
#define REFERENTIAL_IS_BACKTRACE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), REFERENTIAL_TYPE_BACKTRACE))
#define REFERENTIAL_BACKTRACE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), REFERENTIAL_TYPE_BACKTRACE, ReferentialBacktraceClass))


typedef struct _ReferentialBacktrace ReferentialBacktrace;
typedef struct _ReferentialBacktracePrivate ReferentialBacktracePrivate;
typedef struct _ReferentialBacktraceClass ReferentialBacktraceClass;

struct _ReferentialBacktrace
{
    GObject parent;

    ReferentialBacktracePrivate *priv;

    /* Do not add fields to this struct */
};

struct _ReferentialBacktraceClass
{
    GObjectClass parent_class;

    gpointer padding[20];
};

GType referential_backtrace_get_type(void);

ReferentialBacktrace *referential_backtrace_new(guint64 id);

void referential_backtrace_add_symbol(ReferentialBacktrace *backtrace,
					ReferentialSymbol *symbol);

guint64 referential_backtrace_get_id(ReferentialBacktrace *backtrace);
GList *referential_backtrace_get_symbols(ReferentialBacktrace *backtrace);

G_END_DECLS

#endif /* __REFERENTIAL_BACKTRACE_H__ */
