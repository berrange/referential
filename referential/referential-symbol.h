/*
 * referential-symbol.h: gobject watch symbol
 *
 * Copyright (C) 2011 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * Author: Daniel P. Berrange <berrange@redhat.com>
 */

#ifndef __REFERENTIAL_SYMBOL_H__
#define __REFERENTIAL_SYMBOL_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define REFERENTIAL_TYPE_SYMBOL            (referential_symbol_get_type ())
#define REFERENTIAL_SYMBOL(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), REFERENTIAL_TYPE_SYMBOL, ReferentialSymbol))
#define REFERENTIAL_SYMBOL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), REFERENTIAL_TYPE_SYMBOL, ReferentialSymbolClass))
#define REFERENTIAL_IS_SYMBOL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), REFERENTIAL_TYPE_SYMBOL))
#define REFERENTIAL_IS_SYMBOL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), REFERENTIAL_TYPE_SYMBOL))
#define REFERENTIAL_SYMBOL_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), REFERENTIAL_TYPE_SYMBOL, ReferentialSymbolClass))


typedef struct _ReferentialSymbol ReferentialSymbol;
typedef struct _ReferentialSymbolPrivate ReferentialSymbolPrivate;
typedef struct _ReferentialSymbolClass ReferentialSymbolClass;

struct _ReferentialSymbol
{
    GObject parent;

    ReferentialSymbolPrivate *priv;

    /* Do not add fields to this struct */
};

struct _ReferentialSymbolClass
{
    GObjectClass parent_class;

    gpointer padding[20];
};

GType referential_symbol_get_type(void);

ReferentialSymbol *referential_symbol_new(gpointer address,
					     const gchar *loadpath,
					     gpointer loadaddr,
					     const gchar *symname,
					     gpointer symaddr);

gpointer referential_symbol_get_address(ReferentialSymbol *symbol);
const gchar *referential_symbol_get_loadpath(ReferentialSymbol *symbol);
const gchar *referential_symbol_get_loadfile(ReferentialSymbol *symbol);
gpointer referential_symbol_get_loadaddr(ReferentialSymbol *symbol);
const gchar *referential_symbol_get_symname(ReferentialSymbol *symbol);
gpointer referential_symbol_get_symaddr(ReferentialSymbol *symbol);
const gchar *referential_symbol_get_srcpath(ReferentialSymbol *symbol);

void referential_symbol_resolve(ReferentialSymbol *symbol);

G_END_DECLS

#endif /* __REFERENTIAL_SYMBOL_H__ */
