/*
 * referential-parasite.c:  helper APIs for sending data from a parasite
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

#define _GNU_SOURCE
#include <dlfcn.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <execinfo.h>
#include <fnmatch.h>
#include <pthread.h>

#include <glib.h>
#include <glib/gprintf.h>
#include <glib-object.h>
#include <gio/gio.h>
#include <gio/gunixsocketaddress.h>

#include "referential-parasite.h"
#include "referential-protocol.h"

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
static bool initialized;
static GHashTable *objects;
static guint64 objectid = 0;
static GHashTable *traces;
static guint64 traceid = 0;
static GHashTable *symbols;
static GSocket *watchsock;
static gchar rpcbuffer[1024*1024];
static gchar **filters;

static void referential_handle_dump(void)
{
  GHashTableIter iter;
  GObject *obj;

  g_hash_table_iter_init(&iter, objects);
  while (g_hash_table_iter_next(&iter, (gpointer)&obj, NULL)) {
    g_print("0x%016llx %-30s (%u refs)\n",
	    (unsigned long long)obj,
	    G_OBJECT_TYPE_NAME(obj), obj->ref_count);
  }

  g_print("Total objects: %u\n", g_hash_table_size(objects));
  g_print("Total traces: %u\n", g_hash_table_size(traces));
  g_print("Total symbols: %u\n", g_hash_table_size(symbols));
}

static void referential_handle_sigusr1(int signum G_GNUC_UNUSED)
{
  referential_handle_dump();
}

void referential_init(void)
{
  GSocketAddress *addr;
  gchar *path;
  GError *err = NULL;
  gchar *filterstr;

  initialized = FALSE;

  g_type_init();

  objects = g_hash_table_new(NULL, NULL);
  traces = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, NULL);
  symbols = g_hash_table_new(NULL, NULL);

  path = g_strdup_printf("%s/.referential", g_get_home_dir());

  addr = g_unix_socket_address_new(path);

  signal(SIGUSR1, referential_handle_sigusr1);
  atexit(referential_handle_dump);

  watchsock = g_socket_new(G_SOCKET_FAMILY_UNIX,
			   G_SOCKET_TYPE_STREAM,
			   G_SOCKET_PROTOCOL_DEFAULT,
			   &err);
  if (!watchsock) {
    fprintf(stderr, "Cannot create UNIX socket: %s", err->message);
    abort();
  }

  if (!g_socket_connect(watchsock, addr, NULL, &err)) {
    fprintf(stderr, "Cannot connect UNIX socket %s: %s\n",
	    path, err->message);
    g_object_unref(watchsock);
    watchsock = NULL;
  }

  if ((filterstr = getenv("REFERENTIAL_FILTER")) != NULL)
    filters = g_strsplit(filterstr, ";", 0);

  initialized = TRUE;
}


static void referential_send_data(enum referential_message msg,
				  xdrproc_t proc,
				  gpointer args)
{
  XDR xdr;
  referential_hdr hdr;

  if (!watchsock)
    return;

  hdr.length = 0;
  hdr.message = msg;

  xdrmem_create(&xdr,
		rpcbuffer,
		sizeof(rpcbuffer),
		XDR_ENCODE);

  xdr_referential_hdr(&xdr, &hdr);

  (proc)(&xdr, args);

  hdr.length = xdr_getpos(&xdr);
  xdr_setpos(&xdr, 0);
  xdr_referential_hdr(&xdr, &hdr);

  xdr_destroy(&xdr);

  g_socket_send(watchsock, rpcbuffer, hdr.length, NULL, NULL);
} 

static void referential_dispatch_symbol(gpointer sym)
{
  Dl_info info;
  referential_syminfo args;

  if (g_hash_table_lookup(symbols, sym))
    return;

  g_hash_table_insert(symbols, sym, GUINT_TO_POINTER(1));

  if (!dladdr(sym, &info))
    return;

  args.address = (unsigned long long)sym;
  args.loadpath = (char*)(info.dli_fname ? info.dli_fname : "<unknown>");
  args.loadaddr = (unsigned long long)info.dli_fbase;
  args.symname = (char*)(info.dli_sname ? info.dli_sname : "<unknown>");
  args.symaddr = (unsigned long long)info.dli_saddr;

  referential_send_data(REFERENTIAL_MSG_SYMINFO,
			(xdrproc_t)xdr_referential_syminfo,
			&args);
}

static guint64 referential_dispatch_backtrace(gpointer *syms,
					      size_t nsyms)
{
  gchar *key = g_new0(gchar, 17*nsyms);
  size_t i;
  gpointer val;
  guint64 id;
  referential_backtrace bt;

  for (i = 0 ; i < nsyms ; i++) {
    referential_dispatch_symbol(syms[i]);
    g_sprintf(key + 17*i, i == (nsyms-1) ? "%016llx" : "%016llx+" ,
	      (unsigned long long)syms[i]);
  }

  val = g_hash_table_lookup(traces, key);
  if (val) {
    g_free(key);
    return GPOINTER_TO_UINT(val);
  }

  id = traceid++;
  g_hash_table_insert(traces, key, GUINT_TO_POINTER(id));

  bt.id = id;
  bt.frames.frames_len = nsyms;
  bt.frames.frames_val = g_new0(u_quad_t, nsyms);

  for (i = 0 ; i < nsyms ; i++)
    bt.frames.frames_val[i] = (unsigned long long)syms[i];

  referential_send_data(REFERENTIAL_MSG_BACKTRACE,
			(xdrproc_t)xdr_referential_backtrace,
			&bt);

  g_free(bt.frames.frames_val);

  return id;
}

static bool referential_passes_filter(const char *typename)
{
  gchar **tmp = filters;

  if (!filters)
    return TRUE;

  while (tmp && *tmp) {
    if (fnmatch(*tmp, typename, 0) == 0)
      return TRUE;
    tmp++;
  }
  return FALSE;
}

static void referential_dispatch_event(int eventid,
				       void *objaddr,
				       const char *typename,
				       int refs)
{
  gpointer syms[100];
  size_t nsyms = sizeof(syms)/sizeof(syms[0]);
  referential_instance inst;
  referential_event ev;

  if (!initialized)
    return;

  if (eventid == REFERENTIAL_EVENT_NEW &&
      !referential_passes_filter(typename))
    return;

  pthread_mutex_lock(&lock);

  if (eventid == REFERENTIAL_EVENT_NEW) {
    g_hash_table_insert(objects, objaddr, GUINT_TO_POINTER(1));

    inst.addr = (unsigned long long)objaddr;
    inst.id = objectid++;
    inst.klass = (char*)typename;
    referential_send_data(REFERENTIAL_MSG_INSTANCE,
			  (xdrproc_t)xdr_referential_instance,
			  &inst);
  } else if (!g_hash_table_lookup(objects, objaddr)) {
    pthread_mutex_unlock(&lock);
    return;
  }

  nsyms = backtrace(syms, nsyms);

  ev.object = (unsigned long long)objaddr;
  ev.type = eventid;
  if (eventid == REFERENTIAL_EVENT_FREE)
    ev.refs = 0;
  else
    ev.refs = refs;
  ev.backtrace = referential_dispatch_backtrace(syms, nsyms);
  
  referential_send_data(REFERENTIAL_MSG_EVENT,
			(xdrproc_t)xdr_referential_event,
			&ev);

  if (eventid == REFERENTIAL_EVENT_FREE)
    g_hash_table_remove(objects, objaddr);
  
  pthread_mutex_unlock(&lock);
}

void referential_report_new(void *objaddr,
			    const char *typename,
			    int refs)
{
  referential_dispatch_event(REFERENTIAL_EVENT_NEW,
			     objaddr,
			     typename,
			     refs);
}

void referential_report_ref(void *objaddr,
			    int refs)
{
  referential_dispatch_event(REFERENTIAL_EVENT_REF,
			     objaddr,
			     NULL,
			     refs);
}
void referential_report_unref(void *objaddr,
			      int refs)
{
  referential_dispatch_event(REFERENTIAL_EVENT_UNREF,
			     objaddr,
			     NULL,
			     refs);
}
void referential_report_free(void *objaddr)
{
  referential_dispatch_event(REFERENTIAL_EVENT_FREE,
			     objaddr,
			     NULL,
			     0);
}
