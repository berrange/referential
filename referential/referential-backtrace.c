/*
 * referential-backtrace.c: gobject watch backtrace
 *
 * Copyright (C) 2011 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * Author: Daniel P. Berrange <berrange@redhat.com>
 */

#include <string.h>

#include "referential/referential-backtrace.h"

#define REFERENTIAL_BACKTRACE_GET_PRIVATE(obj)                         \
        (G_TYPE_INSTANCE_GET_PRIVATE((obj), REFERENTIAL_TYPE_BACKTRACE, ReferentialBacktracePrivate))

struct _ReferentialBacktracePrivate
{
  guint64 id;
  GList *symbols;
};

G_DEFINE_TYPE(ReferentialBacktrace, referential_backtrace, G_TYPE_OBJECT);

enum {
    PROP_0,
    PROP_ID,    
};


static void referential_backtrace_get_property(GObject *object,
						 guint prop_id,
						 GValue *value,
						 GParamSpec *pspec)
{
    ReferentialBacktrace *backtrace = REFERENTIAL_BACKTRACE(object);
    ReferentialBacktracePrivate *priv = backtrace->priv;

    switch (prop_id) {
    case PROP_ID:
        g_value_set_uint64(value, priv->id);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void referential_backtrace_set_property(GObject *object,
						 guint prop_id,
						 const GValue *value,
						 GParamSpec *pspec)
{
    ReferentialBacktrace *backtrace = REFERENTIAL_BACKTRACE(object);
    ReferentialBacktracePrivate *priv = backtrace->priv;

    switch (prop_id) {
    case PROP_ID:
        priv->id = g_value_get_uint64(value);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void referential_backtrace_finalize(GObject *object)
{
    ReferentialBacktrace *backtrace = REFERENTIAL_BACKTRACE(object);
    ReferentialBacktracePrivate *priv = backtrace->priv;

    g_list_foreach(priv->symbols, (GFunc)g_object_unref, NULL);
    g_list_free(priv->symbols);

    G_OBJECT_CLASS(referential_backtrace_parent_class)->finalize(object);
}


static void referential_backtrace_class_init(ReferentialBacktraceClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = referential_backtrace_finalize;
    object_class->get_property = referential_backtrace_get_property;
    object_class->set_property = referential_backtrace_set_property;

    g_object_class_install_property(object_class,
                                    PROP_ID,
                                    g_param_spec_uint64("id",
							"Trace ID",
							"The trace identifier",
							0,
							G_MAXUINT64,
							0,
							G_PARAM_READABLE |
							G_PARAM_WRITABLE |
							G_PARAM_CONSTRUCT_ONLY |
							G_PARAM_STATIC_NAME |
							G_PARAM_STATIC_NICK |
							G_PARAM_STATIC_BLURB));

    g_type_class_add_private(klass, sizeof(ReferentialBacktracePrivate));
}


static void referential_backtrace_init(ReferentialBacktrace *conn)
{
    ReferentialBacktracePrivate *priv;

    priv = conn->priv = REFERENTIAL_BACKTRACE_GET_PRIVATE(conn);

    memset(priv, 0, sizeof(*priv));
}


ReferentialBacktrace *referential_backtrace_new(guint64 id)
{
    return REFERENTIAL_BACKTRACE(g_object_new(REFERENTIAL_TYPE_BACKTRACE,
						"id", id,
						NULL));
}


void referential_backtrace_add_symbol(ReferentialBacktrace *backtrace,
					ReferentialSymbol *symbol)
{
    ReferentialBacktracePrivate *priv = backtrace->priv;

    g_object_ref(symbol);
    priv->symbols = g_list_append(priv->symbols, symbol);
}


guint64 referential_backtrace_get_id(ReferentialBacktrace *backtrace)
{
    ReferentialBacktracePrivate *priv = backtrace->priv;

    return priv->id;
}


GList *referential_backtrace_get_symbols(ReferentialBacktrace *backtrace)
{
    ReferentialBacktracePrivate *priv = backtrace->priv;

    return g_list_copy(priv->symbols);
}

