/*
 * referential-instance.h: gobject watch instance
 *
 * Copyright (C) 2011 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * Author: Daniel P. Berrange <berrange@redhat.com>
 */

#ifndef __REFERENTIAL_INSTANCE_H__
#define __REFERENTIAL_INSTANCE_H__

#include <glib-object.h>

#include "referential/referential-event.h"

G_BEGIN_DECLS

#define REFERENTIAL_TYPE_INSTANCE            (referential_instance_get_type ())
#define REFERENTIAL_INSTANCE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), REFERENTIAL_TYPE_INSTANCE, ReferentialInstance))
#define REFERENTIAL_INSTANCE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), REFERENTIAL_TYPE_INSTANCE, ReferentialInstanceClass))
#define REFERENTIAL_IS_INSTANCE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), REFERENTIAL_TYPE_INSTANCE))
#define REFERENTIAL_IS_INSTANCE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), REFERENTIAL_TYPE_INSTANCE))
#define REFERENTIAL_INSTANCE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), REFERENTIAL_TYPE_INSTANCE, ReferentialInstanceClass))


typedef struct _ReferentialInstance ReferentialInstance;
typedef struct _ReferentialInstancePrivate ReferentialInstancePrivate;
typedef struct _ReferentialInstanceClass ReferentialInstanceClass;

struct _ReferentialInstance
{
    GObject parent;

    ReferentialInstancePrivate *priv;

    /* Do not add fields to this struct */
};

struct _ReferentialInstanceClass
{
    GObjectClass parent_class;

    gpointer padding[20];
};

GType referential_instance_get_type(void);

ReferentialInstance *referential_instance_new(gpointer address,
					      guint64 id,
					      const gchar *klass);

gpointer referential_instance_get_address(ReferentialInstance *instance);
guint64 referential_instance_get_id(ReferentialInstance *instance);
const gchar *referential_instance_get_klass(ReferentialInstance *instance);
guint referential_instance_get_refs(ReferentialInstance *instance);

void referential_instance_add_event(ReferentialInstance *instance,
				      ReferentialEvent *event);

GList *referential_instance_get_events(ReferentialInstance *instance);

G_END_DECLS

#endif /* __REFERENTIAL_INSTANCE_H__ */
