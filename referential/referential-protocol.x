
enum referential_message {
  REFERENTIAL_MSG_INSTANCE = 0,
  REFERENTIAL_MSG_EVENT = 1,
  REFERENTIAL_MSG_BACKTRACE = 2,
  REFERENTIAL_MSG_SYMINFO = 3
};

struct referential_hdr {
  int length;
  enum referential_message message;
};

const REFERENTIAL_STRING_MAX = 1000;

typedef string referential_string<REFERENTIAL_STRING_MAX>;

enum referential_event_type {
  REFERENTIAL_EVENT_NEW,
  REFERENTIAL_EVENT_REF,
  REFERENTIAL_EVENT_UNREF,
  REFERENTIAL_EVENT_FREE
};

struct referential_instance {
  unsigned hyper addr;
  unsigned hyper id;
  referential_string klass;
};

struct referential_event {
  unsigned hyper object;
  enum referential_event_type type;
  unsigned refs;
  unsigned hyper backtrace;
};

const REFERENTIAL_FRAME_MAX = 100;

struct referential_backtrace {
  unsigned hyper id;
  unsigned hyper frames<REFERENTIAL_FRAME_MAX>;
};

struct referential_syminfo {
  unsigned hyper address;
  referential_string loadpath;
  unsigned hyper loadaddr;
  referential_string symname;
  unsigned hyper symaddr;
};
