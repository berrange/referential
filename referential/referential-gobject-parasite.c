/*
 * referential-gobject-parasite.c: Parasite for GObject instances
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

#define _GNU_SOURCE
#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>

#include <glib-object.h>

#include "referential/referential-parasite.h"

static gpointer (*g_object_ref_ptr)(GObject *obj);
static gpointer (*g_object_unref_ptr)(GObject *obj);
static gpointer (*g_object_new_valist_ptr)(GType type, const char *first, va_list args);

static void referential_gobject_init(void) __attribute__((constructor));

static void referential_gobject_init(void)
{
  if (!(g_object_ref_ptr = dlsym(RTLD_NEXT, "g_object_ref"))) {
    fprintf(stderr, "Cannot locate g_object_ref symbol\n");
    abort();
  }
  if (!(g_object_unref_ptr = dlsym(RTLD_NEXT, "g_object_unref"))) {
    fprintf(stderr, "Cannot locate g_object_unref symbol\n");
    abort();
  }
  if (!(g_object_new_valist_ptr = dlsym(RTLD_NEXT, "g_object_new_valist"))) {
    fprintf(stderr, "Cannot locate g_object_new_valist symbol\n");
    abort();
  }

  referential_init();
}


gpointer g_object_ref(gpointer obj)
{
  gpointer ret = (g_object_ref_ptr)(obj);

  referential_report_ref(obj,
			 g_atomic_int_get(&((GObject*)obj)->ref_count));

  return ret;
}


void g_object_unref(gpointer obj)
{
  (g_object_unref_ptr)(obj);

  referential_report_unref(obj,
			   g_atomic_int_get(&((GObject*)obj)->ref_count));
}

static void handle_free(gpointer data G_GNUC_UNUSED,
			GObject *obj)
{
  referential_report_free(obj);
}

gpointer g_object_new(GType type, const char *first, ...)
{
  va_list args;
  GObject *obj;

  va_start(args, first);
  obj = (g_object_new_valist_ptr)(type, first, args);
  va_end(args);

  referential_report_new(obj,
			 G_OBJECT_TYPE_NAME(obj),
			 g_atomic_int_get(&obj->ref_count));

  g_object_weak_ref(obj, handle_free, NULL);

  return obj;
}
