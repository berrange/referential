/*
 * referential-viewer.c: gobject watch viewer
 *
 * Copyright (C) 2011 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * Author: Daniel P. Berrange <berrange@redhat.com>
 */

#include <string.h>
#include <stdio.h>
#include <glade/glade.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "referential/referential-viewer.h"
#include "referential/referential-protocol.h"

#define REFERENTIAL_VIEWER_GET_PRIVATE(obj)                         \
        (G_TYPE_INSTANCE_GET_PRIVATE((obj), REFERENTIAL_TYPE_VIEWER, ReferentialViewerPrivate))

struct _ReferentialViewerPrivate
{
  GladeXML *glade;

  ReferentialApplication *app;

  ReferentialSession *session;

  GtkTreeStore *model;
};

G_DEFINE_TYPE(ReferentialViewer, referential_viewer, G_TYPE_OBJECT);

enum {
    PROP_0,
    PROP_APPLICATION,
};

static GladeXML *referential_viewer_load_glade(const char *name, const char *widget)
{
	char *path;
	struct stat sb;
	GladeXML *xml;

	if (stat(name, &sb) >= 0)
		return glade_xml_new(name, widget, NULL);

	path = g_strdup_printf("%s/%s", GLADE_DIR, name);

	xml = glade_xml_new(path, widget, NULL);
	g_free(path);
	return xml;
}

static void referential_viewer_get_property(GObject *object,
					      guint prop_id,
					      GValue *value,
					      GParamSpec *pspec)
{
    ReferentialViewer *viewer = REFERENTIAL_VIEWER(object);
    ReferentialViewerPrivate *priv = viewer->priv;

    switch (prop_id) {
    case PROP_APPLICATION:
      g_value_set_object(value, priv->app);
      break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void referential_viewer_set_property(GObject *object,
					      guint prop_id,
					      const GValue *value,
					      GParamSpec *pspec)
{
    ReferentialViewer *viewer = REFERENTIAL_VIEWER(object);
    ReferentialViewerPrivate *priv = viewer->priv;

    switch (prop_id) {
    case PROP_APPLICATION:
      priv->app = g_value_dup_object(value);
      break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void referential_viewer_finalize(GObject *object)
{
    ReferentialViewer *viewer = REFERENTIAL_VIEWER(object);
    ReferentialViewerPrivate *priv = viewer->priv;

    g_object_unref(priv->app);
    g_object_unref(G_OBJECT(priv->glade));

    G_OBJECT_CLASS(referential_viewer_parent_class)->finalize(object);
}


static void referential_viewer_class_init(ReferentialViewerClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = referential_viewer_finalize;
    object_class->get_property = referential_viewer_get_property;
    object_class->set_property = referential_viewer_set_property;

    g_object_class_install_property(object_class,
                                    PROP_APPLICATION,
                                    g_param_spec_object("application",
							"Application",
							"The application name",
							REFERENTIAL_TYPE_APPLICATION,
							G_PARAM_READABLE |
							G_PARAM_WRITABLE |
							G_PARAM_CONSTRUCT_ONLY |
							G_PARAM_STATIC_NAME |
							G_PARAM_STATIC_NICK |
							G_PARAM_STATIC_BLURB));

    g_type_class_add_private(klass, sizeof(ReferentialViewerPrivate));
}


static void referential_viewer_init(ReferentialViewer *conn)
{
    ReferentialViewerPrivate *priv;

    priv = conn->priv = REFERENTIAL_VIEWER_GET_PRIVATE(conn);

    memset(priv, 0, sizeof(*priv));
}


ReferentialViewer *referential_viewer_new(ReferentialApplication *application)
{
    return REFERENTIAL_VIEWER(g_object_new(REFERENTIAL_TYPE_VIEWER,
					     "application", application,
					     NULL));
}


static void referential_viewer_execute_clicked(GtkWidget *src,
						 ReferentialViewer *viewer)
{
  ReferentialViewerPrivate *priv = viewer->priv;

  GError *error = NULL;
  if (!(referential_application_run(priv->app, &error))) {
    fprintf(stderr, "Cannot run '%s': %s\n",
	    referential_application_get_command(priv->app), error->message);
    g_error_free(error);
  }

  gtk_widget_set_sensitive(src, FALSE);

}

static void referential_viewer_refresh_clicked(GtkWidget *src G_GNUC_UNUSED,
						 ReferentialViewer *viewer)
{
  ReferentialViewerPrivate *priv = viewer->priv;
  GList *instList;

  if (priv->session)
    g_object_unref(priv->session);
  priv->session = referential_application_snapshot_session(priv->app);

  gtk_tree_store_clear(priv->model);

  instList = referential_session_get_instances(priv->session);
  while (instList) {
    ReferentialInstance *inst = instList->data;
    GtkTreeIter instIter;
    GList *evList;

    gtk_tree_store_append(priv->model, &instIter, NULL);
    gtk_tree_store_set(priv->model, &instIter, 0, inst, -1);

    evList = referential_instance_get_events(inst);
    while (evList) {
      GtkTreeIter evIter;
      gtk_tree_store_append(priv->model, &evIter, &instIter);
      gtk_tree_store_set(priv->model, &evIter, 0, evList->data, -1);

      evList = evList->next;
    }

    g_list_free(evList);

    instList = instList->next;
  }
  g_list_free(instList);
}

static void cell_data_func_addr(GtkTreeViewColumn *col G_GNUC_UNUSED,
				GtkCellRenderer *cell,
				GtkTreeModel *model,
				GtkTreeIter *iter,
				gpointer data G_GNUC_UNUSED)
{
    GValue val;
    GObject *obj;

    memset(&val, 0, sizeof val);
    gtk_tree_model_get_value(model, iter, 0, &val);
    obj = g_value_get_object(&val);

    if (REFERENTIAL_IS_INSTANCE(obj)) {
      gchar buf[100];
      sprintf(buf, "%llx",
	      (unsigned long long)referential_instance_get_address(REFERENTIAL_INSTANCE(obj)));
      g_object_set(cell, "text", buf, NULL);
    } else {
      g_object_set(cell, "text", "", NULL);
    }
    g_object_unref(obj);
}


static void cell_data_func_id(GtkTreeViewColumn *col G_GNUC_UNUSED,
			      GtkCellRenderer *cell,
			      GtkTreeModel *model,
			      GtkTreeIter *iter,
			      gpointer data G_GNUC_UNUSED)
{
    GValue val;
    GObject *obj;

    memset(&val, 0, sizeof val);
    gtk_tree_model_get_value(model, iter, 0, &val);
    obj = g_value_get_object(&val);

    if (REFERENTIAL_IS_INSTANCE(obj)) {
      gchar buf[100];
      sprintf(buf, "%llu",
	      (unsigned long long)referential_instance_get_id(REFERENTIAL_INSTANCE(obj)));
      g_object_set(cell, "text", buf, NULL);
    } else {
      g_object_set(cell, "text", "", NULL);
    }
    g_object_unref(obj);
}

static void cell_data_func_name(GtkTreeViewColumn *col G_GNUC_UNUSED,
				GtkCellRenderer *cell,
				GtkTreeModel *model,
				GtkTreeIter *iter,
				gpointer data G_GNUC_UNUSED)
{
    GValue val;
    GObject *obj;

    memset(&val, 0, sizeof val);
    gtk_tree_model_get_value(model, iter, 0, &val);
    obj = g_value_get_object(&val);
    if (REFERENTIAL_IS_INSTANCE(obj)) {
      g_object_set(cell, "text",
		   referential_instance_get_klass(REFERENTIAL_INSTANCE(obj)),
		   NULL);
    } else {
      switch (referential_event_get_evtype(REFERENTIAL_EVENT(obj))) {
      case REFERENTIAL_EVENT_NEW:
	g_object_set(cell, "text", "New", NULL);
	break;
      case REFERENTIAL_EVENT_REF:
	g_object_set(cell, "text", "Ref", NULL);
	break;
      case REFERENTIAL_EVENT_UNREF:
	g_object_set(cell, "text", "Unref", NULL);
	break;
      case REFERENTIAL_EVENT_FREE:
	g_object_set(cell, "text", "Free", NULL);
	break;
      default:
	g_object_set(cell, "text", "Unknown", NULL);
	break;
      }
    }

    g_object_unref(obj);
}

static void cell_data_func_refs(GtkTreeViewColumn *col G_GNUC_UNUSED,
				GtkCellRenderer *cell,
				GtkTreeModel *model,
				GtkTreeIter *iter,
				gpointer data G_GNUC_UNUSED)
{
    GValue val;
    GObject *obj;
    gchar buf[100];

    memset(&val, 0, sizeof val);
    gtk_tree_model_get_value(model, iter, 0, &val);
    obj = g_value_get_object(&val);
    if (REFERENTIAL_IS_INSTANCE(obj)) {
      sprintf(buf, "%u", referential_instance_get_refs(REFERENTIAL_INSTANCE(obj)));
    } else {
      sprintf(buf, "%u", referential_event_get_refs(REFERENTIAL_EVENT(obj)));
    }
    g_object_set(cell, "text", buf, NULL);
    g_object_unref(obj);
}


static GObject *
referential_viewer_get_selected_object(ReferentialViewer *viewer,
					 GtkTreeSelection *sel)
{
  ReferentialViewerPrivate *priv = viewer->priv;
  GtkTreeIter iter;
  gboolean selected;
  GValue val;

  selected = gtk_tree_selection_get_selected(sel, NULL, &iter);
  if (!selected)
    return NULL;

  memset(&val, 0, sizeof val);
  gtk_tree_model_get_value(GTK_TREE_MODEL(priv->model), &iter, 0, &val);
  return g_value_get_object(&val);
}


static void
referential_viewer_object_select(GtkTreeSelection *selection,
				   gpointer data)
{
  ReferentialViewer *viewer = data;
  ReferentialViewerPrivate *priv = viewer->priv;
  GObject *obj = referential_viewer_get_selected_object(viewer, selection);
  GString *str = g_string_new("");
  ReferentialEvent *ev = NULL;

  if (obj && REFERENTIAL_IS_INSTANCE(obj)) {
    ReferentialInstance *inst = REFERENTIAL_INSTANCE(obj);
    GList *evList = referential_instance_get_events(inst);
    ev = evList ? evList->data : NULL;
    g_list_free(evList);
  } else if (obj && REFERENTIAL_IS_EVENT(obj)) {
    ev = REFERENTIAL_EVENT(obj);
  }

  if (ev) {
    ReferentialBacktrace *bt;
    GList *tmp;
    bt = referential_event_get_backtrace(ev);
    tmp = referential_backtrace_get_symbols(bt);
    while (tmp) {
      ReferentialSymbol *sym = tmp->data;

      if (!strstr(referential_symbol_get_loadpath(sym),
		  "libreferential.so")) {
	referential_symbol_resolve(sym);
	g_string_append_printf(str, "%016llx in %s() at %s\n",
			       (unsigned long long)referential_symbol_get_address(sym),
			       referential_symbol_get_symname(sym),
			       referential_symbol_get_srcpath(sym));
      }

      if (strcmp(referential_symbol_get_symname(sym), "main") == 0)
	break;

      tmp = tmp->next;
    }
  }

  GtkWidget *bttext = glade_xml_get_widget(priv->glade, "backtrace");
  GtkTextBuffer *buf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(bttext));
  gtk_text_buffer_set_text(buf, str->str, str->len);
  g_string_free(str, TRUE);
}

static gint cell_data_sort_name(GtkTreeModel *model,
				GtkTreeIter *a,
				GtkTreeIter *b,
				gpointer data G_GNUC_UNUSED)
{
    GObject *obja;
    GObject *objb;
    GValue vala;
    GValue valb;
    gint cmp;

    memset(&vala, 0, sizeof vala);
    memset(&valb, 0, sizeof valb);
    gtk_tree_model_get_value(model, a, 0, &vala);
    gtk_tree_model_get_value(model, b, 0, &valb);
    obja = g_value_get_object(&vala);
    objb = g_value_get_object(&valb);
  
    if (REFERENTIAL_IS_INSTANCE(obja) &&
	REFERENTIAL_IS_INSTANCE(objb))
      cmp = strcmp(referential_instance_get_klass(REFERENTIAL_INSTANCE(obja)),
		   referential_instance_get_klass(REFERENTIAL_INSTANCE(objb)));
    else
      cmp = 0;
	  
    g_object_unref(obja);
    g_object_unref(objb);

    return cmp;
}


static gint cell_data_sort_addr(GtkTreeModel *model,
				GtkTreeIter *a,
				GtkTreeIter *b,
				gpointer data G_GNUC_UNUSED)
{
    GObject *obja;
    GObject *objb;
    GValue vala;
    GValue valb;
    gint cmp;

    memset(&vala, 0, sizeof vala);
    memset(&valb, 0, sizeof valb);
    gtk_tree_model_get_value(model, a, 0, &vala);
    gtk_tree_model_get_value(model, b, 0, &valb);
    obja = g_value_get_object(&vala);
    objb = g_value_get_object(&valb);
  
    if (REFERENTIAL_IS_INSTANCE(obja) &&
	REFERENTIAL_IS_INSTANCE(objb)) {
      long long diff;
      diff = (long long)referential_instance_get_address(REFERENTIAL_INSTANCE(obja)) -
	(long long)referential_instance_get_address(REFERENTIAL_INSTANCE(objb));
      if (diff < 0)
	cmp = -1;
      else if (diff > 0)
	cmp = 1;
      else
	cmp = 0;
    } else {
      cmp = 0;
    }
	  
    g_object_unref(obja);
    g_object_unref(objb);

    return cmp;
}


static gint cell_data_sort_id(GtkTreeModel *model,
			      GtkTreeIter *a,
			      GtkTreeIter *b,
			      gpointer data G_GNUC_UNUSED)
{
    GObject *obja;
    GObject *objb;
    GValue vala;
    GValue valb;
    gint cmp;

    memset(&vala, 0, sizeof vala);
    memset(&valb, 0, sizeof valb);
    gtk_tree_model_get_value(model, a, 0, &vala);
    gtk_tree_model_get_value(model, b, 0, &valb);
    obja = g_value_get_object(&vala);
    objb = g_value_get_object(&valb);
  
    if (REFERENTIAL_IS_INSTANCE(obja) &&
	REFERENTIAL_IS_INSTANCE(objb)) {
      long long diff;
      diff = (long long)referential_instance_get_id(REFERENTIAL_INSTANCE(obja)) -
	(long long)referential_instance_get_id(REFERENTIAL_INSTANCE(objb));
      if (diff < 0)
	cmp = -1;
      else if (diff > 0)
	cmp = 1;
      else
	cmp = 0;
    } else {
      cmp = 0;
    }
	  
    g_object_unref(obja);
    g_object_unref(objb);

    return cmp;
}


static gint cell_data_sort_refs(GtkTreeModel *model,
				GtkTreeIter *a,
				GtkTreeIter *b,
				gpointer data G_GNUC_UNUSED)
{
    GObject *obja;
    GObject *objb;
    GValue vala;
    GValue valb;
    gint cmp;

    memset(&vala, 0, sizeof vala);
    memset(&valb, 0, sizeof valb);
    gtk_tree_model_get_value(model, a, 0, &vala);
    gtk_tree_model_get_value(model, b, 0, &valb);
    obja = g_value_get_object(&vala);
    objb = g_value_get_object(&valb);
  
    if (REFERENTIAL_IS_INSTANCE(obja) &&
	REFERENTIAL_IS_INSTANCE(objb)) {
      cmp = referential_instance_get_refs(REFERENTIAL_INSTANCE(obja)) -
	referential_instance_get_refs(REFERENTIAL_INSTANCE(objb));
      if (cmp == 0)
	cmp = strcmp(referential_instance_get_klass(REFERENTIAL_INSTANCE(obja)),
		     referential_instance_get_klass(REFERENTIAL_INSTANCE(objb)));
    } else {
      cmp = 0;
    }
	  
    g_object_unref(obja);
    g_object_unref(objb);

    return cmp;
}


gboolean referential_viewer_show(ReferentialViewer *viewer)
{
  ReferentialViewerPrivate *priv = viewer->priv;
  GtkWidget *dialog;
  GtkCellRenderer *addr;
  GtkCellRenderer *name;
  GtkCellRenderer *refs;
  GtkCellRenderer *id;
  GtkTreeViewColumn *addrCol;
  GtkTreeViewColumn *nameCol;
  GtkTreeViewColumn *refsCol;
  GtkTreeViewColumn *idCol;
  GtkTreeSelection *objectSel;
  GtkWidget *list;
  PangoFontDescription *font;
  GtkWidget *backtrace;

  priv->glade = referential_viewer_load_glade("referential.glade", "window");

  dialog = glade_xml_get_widget(priv->glade, "window");
  glade_xml_signal_connect_data(priv->glade, "on_quit_clicked",
				G_CALLBACK(gtk_main_quit), NULL);
  glade_xml_signal_connect_data(priv->glade, "on_execute_clicked",
				G_CALLBACK(referential_viewer_execute_clicked), viewer);
  glade_xml_signal_connect_data(priv->glade, "on_refresh_clicked",
				G_CALLBACK(referential_viewer_refresh_clicked), viewer);
  g_signal_connect(dialog, "delete-event", 
		   G_CALLBACK(gtk_main_quit), NULL);


  list = glade_xml_get_widget(priv->glade, "object-list");
  objectSel = gtk_tree_view_get_selection(GTK_TREE_VIEW(list));

  priv->model = gtk_tree_store_new(1, G_TYPE_OBJECT);

  addr = gtk_cell_renderer_text_new();
  id = gtk_cell_renderer_text_new();
  name = gtk_cell_renderer_text_new();
  refs = gtk_cell_renderer_text_new();

  addrCol = gtk_tree_view_column_new_with_attributes("Address", addr, NULL);
  idCol = gtk_tree_view_column_new_with_attributes("ID", id, NULL);
  nameCol = gtk_tree_view_column_new_with_attributes("Name", name, NULL);
  refsCol = gtk_tree_view_column_new_with_attributes("Refs", refs, NULL);

  g_object_set(addrCol, "expand", FALSE, NULL);
  g_object_set(idCol, "expand", FALSE, NULL);
  g_object_set(nameCol, "expand", TRUE, NULL);
  g_object_set(refsCol, "expand", FALSE, NULL);

  gtk_tree_view_append_column(GTK_TREE_VIEW(list), idCol);
  gtk_tree_view_append_column(GTK_TREE_VIEW(list), addrCol);
  gtk_tree_view_append_column(GTK_TREE_VIEW(list), nameCol);
  gtk_tree_view_append_column(GTK_TREE_VIEW(list), refsCol);

  gtk_tree_view_column_set_cell_data_func(addrCol, addr,
					  cell_data_func_addr, NULL, NULL);
  gtk_tree_view_column_set_cell_data_func(idCol, id,
					  cell_data_func_id, NULL, NULL);
  gtk_tree_view_column_set_cell_data_func(nameCol, name,
					  cell_data_func_name, NULL, NULL);
  gtk_tree_view_column_set_cell_data_func(refsCol, refs,
					  cell_data_func_refs, NULL, NULL);

  gtk_tree_view_column_set_sort_column_id(addrCol, 1);
  gtk_tree_view_column_set_sort_column_id(idCol, 2);
  gtk_tree_view_column_set_sort_column_id(nameCol, 3);
  gtk_tree_view_column_set_sort_column_id(refsCol, 4);
  gtk_tree_sortable_set_sort_func(GTK_TREE_SORTABLE(priv->model),
				  1,
				  cell_data_sort_addr,
				  NULL, NULL);
  gtk_tree_sortable_set_sort_func(GTK_TREE_SORTABLE(priv->model),
				  2,
				  cell_data_sort_id,
				  NULL, NULL);
  gtk_tree_sortable_set_sort_func(GTK_TREE_SORTABLE(priv->model),
				  3,
				  cell_data_sort_name,
				  NULL, NULL);
  gtk_tree_sortable_set_sort_func(GTK_TREE_SORTABLE(priv->model),
				  4,
				  cell_data_sort_refs,
				  NULL, NULL);
  gtk_tree_sortable_set_default_sort_func(GTK_TREE_SORTABLE(priv->model),
					  cell_data_sort_id,
					  NULL, NULL);
  gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(priv->model),
				       GTK_TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID,
				       GTK_SORT_ASCENDING);

  gtk_tree_view_set_model(GTK_TREE_VIEW(list), GTK_TREE_MODEL(priv->model));


  backtrace = glade_xml_get_widget(priv->glade, "backtrace");
  font = pango_font_description_from_string("Monospace 10");
  gtk_widget_modify_font(backtrace, font);
  pango_font_description_free(font);


  g_signal_connect(objectSel, "changed", G_CALLBACK(referential_viewer_object_select), viewer);

  gtk_widget_show_all(dialog);

  return TRUE;
}

