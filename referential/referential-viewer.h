/*
 * referential-viewer.h: gobject watch viewer
 *
 * Copyright (C) 2011 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * Author: Daniel P. Berrange <berrange@redhat.com>
 */

#ifndef __REFERENTIAL_VIEWER_H__
#define __REFERENTIAL_VIEWER_H__

#include <glib-object.h>

#include "referential/referential-application.h"

G_BEGIN_DECLS

#define REFERENTIAL_TYPE_VIEWER            (referential_viewer_get_type ())
#define REFERENTIAL_VIEWER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), REFERENTIAL_TYPE_VIEWER, ReferentialViewer))
#define REFERENTIAL_VIEWER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), REFERENTIAL_TYPE_VIEWER, ReferentialViewerClass))
#define REFERENTIAL_IS_VIEWER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), REFERENTIAL_TYPE_VIEWER))
#define REFERENTIAL_IS_VIEWER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), REFERENTIAL_TYPE_VIEWER))
#define REFERENTIAL_VIEWER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), REFERENTIAL_TYPE_VIEWER, ReferentialViewerClass))


typedef struct _ReferentialViewer ReferentialViewer;
typedef struct _ReferentialViewerPrivate ReferentialViewerPrivate;
typedef struct _ReferentialViewerClass ReferentialViewerClass;

struct _ReferentialViewer
{
    GObject parent;

    ReferentialViewerPrivate *priv;

    /* Do not add fields to this struct */
};

struct _ReferentialViewerClass
{
    GObjectClass parent_class;

    gpointer padding[20];
};

GType referential_viewer_get_type(void);

ReferentialViewer *referential_viewer_new(ReferentialApplication *app);

gboolean referential_viewer_show(ReferentialViewer *viewer);

G_END_DECLS

#endif /* __REFERENTIAL_VIEWER_H__ */
