
#include <gtk/gtk.h>

#include "referential/referential-viewer.h"

int main(int argc, char **argv)
{
  GOptionContext *context;
  GError *error = NULL;
  gchar *filter = NULL;
  gchar *parasite = NULL;
  gchar **args = NULL;
  gboolean autorun = FALSE;
  const GOptionEntry options[] = {
    { "parasite", 'p', 0, G_OPTION_ARG_STRING, &parasite,
      "parasite name", "STRING" },
    { "filter", 'f', 0, G_OPTION_ARG_STRING, &filter,
      "object class filter", "REGEX"},
    { "autorun", 'r', 0, G_OPTION_ARG_NONE, &autorun,
      "automatically run command", ""},
    { G_OPTION_REMAINING, '\0', 0, G_OPTION_ARG_STRING_ARRAY, &args,
      NULL, "[command][args..]" },
    { NULL, 0, 0, G_OPTION_ARG_NONE, NULL, NULL, 0 }
  };
  int i;
  gchar *command;
  ReferentialApplication *app;
  ReferentialViewer *viewer;
  int ret = 1;
  
  context = g_option_context_new("- GObject Watch");
  g_option_context_add_main_entries(context, options, NULL);
  g_option_context_add_group(context, gtk_get_option_group (TRUE));
  g_option_context_parse(context, &argc, &argv, &error);

  if (error) {
    g_print ("%s\n - Run %s --help for full command usage\n",
	     error->message, argv[0]);
    g_error_free (error);
    return 1;
  }
  if (!args) {
    fprintf(stderr, "Usage: %s [command] [args..]\n - Run %s --help for full command usage\n",
	    argv[0], argv[0]);
    return 1;
  }

  for (i = 0 ; args[i] != NULL ; i++) {
    gchar *arg = args[i];
    args[i] = g_shell_quote(arg);
    g_free(arg);
  }

  command = g_strjoinv(" ", args);
  g_strfreev(args);
  app = referential_application_new(command,
				    parasite ? parasite : "gobject",
				    filter);
  g_free(command);
  viewer = referential_viewer_new(app);

  if (autorun &&
      !(referential_application_run(app, &error))) {
    fprintf(stderr, "Cannot run '%s': %s\n",
	    referential_application_get_command(app), error->message);
    g_error_free(error);
    return FALSE;
  }

  if (!(referential_viewer_show(viewer)))
    goto cleanup;

  ret = 0;
  gtk_main();

 cleanup:
  g_object_unref(app);
  g_object_unref(viewer);
  g_free(filter);
  g_free(parasite);
  return ret;
}
