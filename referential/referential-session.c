/*
 * referential-session.c: gobject watch session
 *
 * Copyright (C) 2011 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * Author: Daniel P. Berrange <berrange@redhat.com>
 */

#include "referential/referential-session.h"
#include "referential/referential-protocol.h"

#define REFERENTIAL_SESSION_GET_PRIVATE(obj)                         \
        (G_TYPE_INSTANCE_GET_PRIVATE((obj), REFERENTIAL_TYPE_SESSION, ReferentialSessionPrivate))

struct _ReferentialSessionPrivate
{
  GHashTable *objects;
  GHashTable *backtraces;
  GHashTable *symbols;
};

G_DEFINE_TYPE(ReferentialSession, referential_session, G_TYPE_OBJECT);

enum {
    PROP_0,
};


static void referential_session_get_property(GObject *object,
					     guint prop_id,
					     GValue *value G_GNUC_UNUSED,
					     GParamSpec *pspec)
{
    switch (prop_id) {
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void referential_session_set_property(GObject *object,
					       guint prop_id,
					       const GValue *value G_GNUC_UNUSED,
					       GParamSpec *pspec)
{
    switch (prop_id) {
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void referential_session_finalize(GObject *object)
{
    ReferentialSession *session = REFERENTIAL_SESSION(object);
    ReferentialSessionPrivate *priv = session->priv;

    g_hash_table_unref(priv->objects);
    g_hash_table_unref(priv->backtraces);
    g_hash_table_unref(priv->symbols);

    G_OBJECT_CLASS(referential_session_parent_class)->finalize(object);
}


static void referential_session_class_init(ReferentialSessionClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = referential_session_finalize;
    object_class->get_property = referential_session_get_property;
    object_class->set_property = referential_session_set_property;

    g_type_class_add_private(klass, sizeof(ReferentialSessionPrivate));
}


static void referential_session_init(ReferentialSession *conn)
{
    ReferentialSessionPrivate *priv;

    priv = conn->priv = REFERENTIAL_SESSION_GET_PRIVATE(conn);

    memset(priv, 0, sizeof(*priv));

    priv->objects = g_hash_table_new_full(NULL, NULL,
					  NULL, g_object_unref);
    priv->backtraces = g_hash_table_new_full(g_int64_hash, g_int64_equal,
					     g_free, g_object_unref);
    priv->symbols = g_hash_table_new_full(NULL, NULL,
					  NULL, g_object_unref);
}


ReferentialSession *referential_session_new(void)
{
    return REFERENTIAL_SESSION(g_object_new(REFERENTIAL_TYPE_SESSION,
					      NULL));
}


ReferentialSession *referential_session_snapshot(ReferentialSession *session)
{
  ReferentialSessionPrivate *priv = session->priv;
  ReferentialSession *newsession = referential_session_new();
  GHashTableIter iter;
  gpointer key, val;

  g_hash_table_iter_init(&iter, priv->objects);
  while (g_hash_table_iter_next(&iter, &key, &val))
    referential_session_add_instance(newsession, val);

  g_hash_table_iter_init(&iter, priv->backtraces);
  while (g_hash_table_iter_next(&iter, &key, &val))
    referential_session_add_backtrace(newsession, val);

  g_hash_table_iter_init(&iter, priv->symbols);
  while (g_hash_table_iter_next(&iter, &key, &val))
    referential_session_add_symbol(newsession, val);

  return newsession;
}


void referential_session_add_instance(ReferentialSession *session,
					ReferentialInstance *inst)
{
    ReferentialSessionPrivate *priv = session->priv;

    g_object_ref(inst);
    g_hash_table_insert(priv->objects, referential_instance_get_address(inst), inst);
}

void referential_session_add_backtrace(ReferentialSession *session,
					 ReferentialBacktrace *backtrace)
{
    ReferentialSessionPrivate *priv = session->priv;
    guint64 *key = g_new0(guint64, 1);
    *key = referential_backtrace_get_id(backtrace);

    g_object_ref(backtrace);
    g_hash_table_insert(priv->backtraces, key, backtrace);
}

void referential_session_add_symbol(ReferentialSession *session,
				      ReferentialSymbol *syminfo)
{
    ReferentialSessionPrivate *priv = session->priv;

    g_object_ref(syminfo);
    g_hash_table_insert(priv->symbols, referential_symbol_get_address(syminfo), syminfo);
}


ReferentialInstance *referential_session_find_instance(ReferentialSession *session,
							  gpointer address)
{
    ReferentialSessionPrivate *priv = session->priv;

    return g_hash_table_lookup(priv->objects, address);
}


ReferentialBacktrace *referential_session_find_backtrace(ReferentialSession *session,
							    guint64 id)
{
    ReferentialSessionPrivate *priv = session->priv;

    return g_hash_table_lookup(priv->backtraces, &id);
}


ReferentialSymbol *referential_session_find_symbol(ReferentialSession *session,
						      gpointer address)
{
    ReferentialSessionPrivate *priv = session->priv;

    return g_hash_table_lookup(priv->symbols, address);
}


GList *referential_session_get_instances(ReferentialSession *session)
{
    ReferentialSessionPrivate *priv = session->priv;

    return g_hash_table_get_values(priv->objects);
}
