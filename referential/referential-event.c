/*
 * referential-event.c: gobject watch event
 *
 * Copyright (C) 2011 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * Author: Daniel P. Berrange <berrange@redhat.com>
 */

#include "referential/referential-event.h"
#include "referential/referential-protocol.h"

#define REFERENTIAL_EVENT_GET_PRIVATE(obj)                         \
        (G_TYPE_INSTANCE_GET_PRIVATE((obj), REFERENTIAL_TYPE_EVENT, ReferentialEventPrivate))

struct _ReferentialEventPrivate
{
  int evtype;
  guint refs;
  ReferentialBacktrace *backtrace;
};

G_DEFINE_TYPE(ReferentialEvent, referential_event, G_TYPE_OBJECT);

enum {
    PROP_0,
    PROP_EVTYPE,
    PROP_REFS,
    PROP_BACKTRACE,
};


static void referential_event_get_property(GObject *object,
					     guint prop_id,
					     GValue *value,
					     GParamSpec *pspec)
{
    ReferentialEvent *event = REFERENTIAL_EVENT(object);
    ReferentialEventPrivate *priv = event->priv;

    switch (prop_id) {
    case PROP_EVTYPE:
        g_value_set_int(value, priv->evtype);
        break;

    case PROP_REFS:
        g_value_set_uint(value, priv->refs);
        break;

    case PROP_BACKTRACE:
        g_value_set_object(value, priv->backtrace);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void referential_event_set_property(GObject *object,
					     guint prop_id,
					     const GValue *value,
					     GParamSpec *pspec)
{
    ReferentialEvent *event = REFERENTIAL_EVENT(object);
    ReferentialEventPrivate *priv = event->priv;

    switch (prop_id) {
    case PROP_EVTYPE:
        priv->evtype = g_value_get_int(value);
        break;

    case PROP_REFS:
        priv->refs = g_value_get_uint(value);
        break;

    case PROP_BACKTRACE:
        priv->backtrace = g_value_dup_object(value);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void referential_event_finalize(GObject *object)
{
    ReferentialEvent *event = REFERENTIAL_EVENT(object);
    ReferentialEventPrivate *priv = event->priv;

    g_object_unref(priv->backtrace);

    G_OBJECT_CLASS(referential_event_parent_class)->finalize(object);
}


static void referential_event_class_init(ReferentialEventClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = referential_event_finalize;
    object_class->get_property = referential_event_get_property;
    object_class->set_property = referential_event_set_property;

    g_object_class_install_property(object_class,
                                    PROP_EVTYPE,
                                    g_param_spec_int("evtype",
						     "Event type",
						     "The event type",
						     REFERENTIAL_MSG_INSTANCE,
						     REFERENTIAL_MSG_SYMINFO,
						     REFERENTIAL_MSG_INSTANCE,
						     G_PARAM_READABLE |
						     G_PARAM_WRITABLE |
						     G_PARAM_CONSTRUCT_ONLY |
						     G_PARAM_STATIC_NAME |
						     G_PARAM_STATIC_NICK |
						     G_PARAM_STATIC_BLURB));
    g_object_class_install_property(object_class,
                                    PROP_REFS,
                                    g_param_spec_uint("refs",
						      "Refs",
						      "The reference count",
						      0,
						      G_MAXUINT,
						      0,
						      G_PARAM_READABLE |
						      G_PARAM_WRITABLE |
						      G_PARAM_CONSTRUCT_ONLY |
						      G_PARAM_STATIC_NAME |
						      G_PARAM_STATIC_NICK |
						      G_PARAM_STATIC_BLURB));
    g_object_class_install_property(object_class,
                                    PROP_BACKTRACE,
                                    g_param_spec_object("backtrace",
							"Backtrace",
							"Stack backtrace",
							REFERENTIAL_TYPE_BACKTRACE,
							G_PARAM_READABLE |
							G_PARAM_WRITABLE |
							G_PARAM_CONSTRUCT_ONLY |
							G_PARAM_STATIC_NAME |
							G_PARAM_STATIC_NICK |
							G_PARAM_STATIC_BLURB));

    g_type_class_add_private(klass, sizeof(ReferentialEventPrivate));
}


static void referential_event_init(ReferentialEvent *conn)
{
    ReferentialEventPrivate *priv;

    priv = conn->priv = REFERENTIAL_EVENT_GET_PRIVATE(conn);

    memset(priv, 0, sizeof(*priv));
}


ReferentialEvent *referential_event_new(int evtype,
					   guint refs,
					   ReferentialBacktrace *backtrace)
{
    return REFERENTIAL_EVENT(g_object_new(REFERENTIAL_TYPE_EVENT,
					    "evtype", evtype,
					    "refs", refs,
					    "backtrace", backtrace,
					    NULL));
}

int referential_event_get_evtype(ReferentialEvent *event)
{
    ReferentialEventPrivate *priv = event->priv;

    return priv->evtype;
}

guint referential_event_get_refs(ReferentialEvent *event)
{
    ReferentialEventPrivate *priv = event->priv;

    return priv->refs;
}

ReferentialBacktrace *referential_event_get_backtrace(ReferentialEvent *event)
{
    ReferentialEventPrivate *priv = event->priv;

    return priv->backtrace;
}
