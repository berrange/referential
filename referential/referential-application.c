/*
 * referential-application.c: gobject watch application
 *
 * Copyright (C) 2011 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * Author: Daniel P. Berrange <berrange@redhat.com>
 */

#include <gio/gio.h>
#include <gio/gunixsocketaddress.h>

#include "referential/referential-application.h"
#include "referential/referential-protocol.h"

#define REFERENTIAL_APPLICATION_GET_PRIVATE(obj)                         \
        (G_TYPE_INSTANCE_GET_PRIVATE((obj), REFERENTIAL_TYPE_APPLICATION, ReferentialApplicationPrivate))

struct _ReferentialApplicationPrivate
{
  gchar *command;
  gchar *parasite;
  gchar *filter;

  ReferentialSession *session;

  GSocket *lsock;
  GSocket *csock;
};

G_DEFINE_TYPE(ReferentialApplication, referential_application, G_TYPE_OBJECT);

enum {
    PROP_0,
    PROP_COMMAND,
    PROP_PARASITE,
    PROP_FILTER,
};


static void referential_application_get_property(GObject *object,
						   guint prop_id,
						   GValue *value,
						   GParamSpec *pspec)
{
    ReferentialApplication *application = REFERENTIAL_APPLICATION(object);
    ReferentialApplicationPrivate *priv = application->priv;

    switch (prop_id) {
    case PROP_COMMAND:
      g_value_set_string(value, priv->command);
      break;

    case PROP_PARASITE:
      g_value_set_string(value, priv->parasite);
      break;

    case PROP_FILTER:
      g_value_set_string(value, priv->filter);
      break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void referential_application_set_property(GObject *object,
						   guint prop_id,
						   const GValue *value,
						   GParamSpec *pspec)
{
    ReferentialApplication *application = REFERENTIAL_APPLICATION(object);
    ReferentialApplicationPrivate *priv = application->priv;

    switch (prop_id) {
    case PROP_COMMAND:
      priv->command = g_value_dup_string(value);
      break;

    case PROP_PARASITE:
      priv->parasite = g_value_dup_string(value);
      break;

    case PROP_FILTER:
      priv->filter = g_value_dup_string(value);
      break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void referential_application_finalize(GObject *object)
{
    ReferentialApplication *application = REFERENTIAL_APPLICATION(object);
    ReferentialApplicationPrivate *priv = application->priv;

    g_free(priv->filter);
    g_free(priv->parasite);
    g_free(priv->command);

    g_object_unref(priv->session);
    if (priv->lsock)
      g_object_unref(priv->lsock);
    if (priv->csock)
      g_object_unref(priv->csock);

    G_OBJECT_CLASS(referential_application_parent_class)->finalize(object);
}


static void referential_application_class_init(ReferentialApplicationClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = referential_application_finalize;
    object_class->get_property = referential_application_get_property;
    object_class->set_property = referential_application_set_property;

    g_object_class_install_property(object_class,
                                    PROP_COMMAND,
                                    g_param_spec_string("command",
							"Command",
							"The command name",
							NULL,
							G_PARAM_READABLE |
							G_PARAM_WRITABLE |
							G_PARAM_CONSTRUCT_ONLY |
							G_PARAM_STATIC_NAME |
							G_PARAM_STATIC_NICK |
							G_PARAM_STATIC_BLURB));
    g_object_class_install_property(object_class,
                                    PROP_PARASITE,
                                    g_param_spec_string("parasite",
							"Parasite",
							"The parasite name",
							NULL,
							G_PARAM_READABLE |
							G_PARAM_WRITABLE |
							G_PARAM_CONSTRUCT_ONLY |
							G_PARAM_STATIC_NAME |
							G_PARAM_STATIC_NICK |
							G_PARAM_STATIC_BLURB));
    g_object_class_install_property(object_class,
                                    PROP_FILTER,
                                    g_param_spec_string("filter",
							"Filter",
							"The filter name",
							NULL,
							G_PARAM_READABLE |
							G_PARAM_WRITABLE |
							G_PARAM_CONSTRUCT_ONLY |
							G_PARAM_STATIC_NAME |
							G_PARAM_STATIC_NICK |
							G_PARAM_STATIC_BLURB));

    g_type_class_add_private(klass, sizeof(ReferentialApplicationPrivate));
}


static void referential_application_init(ReferentialApplication *conn)
{
    ReferentialApplicationPrivate *priv;

    priv = conn->priv = REFERENTIAL_APPLICATION_GET_PRIVATE(conn);

    memset(priv, 0, sizeof(*priv));

    priv->session = referential_session_new();
}


ReferentialApplication *referential_application_new(const gchar *command,
						    const gchar *parasite,
						    const gchar *filter)
{
    return REFERENTIAL_APPLICATION(g_object_new(REFERENTIAL_TYPE_APPLICATION,
						"command", command,
						"parasite", parasite,
						"filter", filter,
						NULL));
}

ReferentialSession *referential_application_snapshot_session(ReferentialApplication *app)
{
  ReferentialApplicationPrivate *priv = app->priv;

  return referential_session_snapshot(priv->session);
}


static gboolean referential_application_read(GSocket *csock G_GNUC_UNUSED,
					       GIOCondition cond G_GNUC_UNUSED,
					       gpointer data)
{
  ReferentialApplication *app = data;
  ReferentialApplicationPrivate *priv = app->priv;
  static gchar rpcbuffer[1024*1024];
  XDR xdr;
  referential_hdr hdr;

  if (!g_socket_receive(priv->csock,
			rpcbuffer,
			8, NULL, NULL))
      goto error;

  xdrmem_create(&xdr,
		rpcbuffer,
		sizeof(rpcbuffer),
		XDR_DECODE);

  xdr_referential_hdr(&xdr, &hdr);

  if (!g_socket_receive(csock,
			rpcbuffer + 8,
			hdr.length - 8,
			NULL, NULL))
    goto error;

  switch (hdr.message) {
  case REFERENTIAL_MSG_INSTANCE: {
    referential_instance args;
    memset(&args, 0, sizeof(args));
    xdr_referential_instance(&xdr, (gpointer)&args);

    ReferentialInstance *inst = referential_instance_new((gpointer)args.addr,
							 args.id,
							 args.klass);

    referential_session_add_instance(priv->session, inst);

    g_object_unref(inst);

    xdr_free((xdrproc_t)xdr_referential_instance, (gpointer)&args);
  } break;

  case REFERENTIAL_MSG_EVENT: {
    referential_event args;
    memset(&args, 0, sizeof(args));
    xdr_referential_event(&xdr, (gpointer)&args);

    ReferentialInstance *inst = referential_session_find_instance(priv->session,
								     (gpointer)args.object);
    ReferentialBacktrace *bt = referential_session_find_backtrace(priv->session,
								     args.backtrace);

    ReferentialEvent *ev = referential_event_new(args.type,
						    args.refs,
						    bt);

    referential_instance_add_event(inst, ev);

    g_object_unref(ev);

    xdr_free((xdrproc_t)xdr_referential_event, (gpointer)&args);
  } break;

  case REFERENTIAL_MSG_BACKTRACE: {
    referential_backtrace args;
    memset(&args, 0, sizeof(args));
    xdr_referential_backtrace(&xdr, (gpointer)&args);

    ReferentialBacktrace *bt = referential_backtrace_new(args.id);
    int i;
    for (i = 0 ; i < args.frames.frames_len ; i++) {
      ReferentialSymbol *sym = referential_session_find_symbol(priv->session,
								  (gpointer)args.frames.frames_val[i]
								  );

      if (!sym) {
	fprintf(stderr, "Missing sym %p\n", (gpointer)args.frames.frames_val[i]);
      } else {
	referential_backtrace_add_symbol(bt, sym);
      }
    }

    referential_session_add_backtrace(priv->session, bt);

    g_object_unref(bt);

    xdr_free((xdrproc_t)xdr_referential_backtrace, (gpointer)&args);
  } break;

  case REFERENTIAL_MSG_SYMINFO: {
    referential_syminfo args;
    memset(&args, 0, sizeof(args));
    xdr_referential_syminfo(&xdr, (gpointer)&args);

    ReferentialSymbol *sym = referential_symbol_new((gpointer)args.address,
						       args.loadpath,
						       (gpointer)args.loadaddr,
						       args.symname,
						       (gpointer)args.symaddr);
    referential_session_add_symbol(priv->session, sym);

    g_object_unref(sym);

    xdr_free((xdrproc_t)xdr_referential_syminfo, (gpointer)&args);
  } break;
  }

  xdr_destroy(&xdr);

  return TRUE;

 error:
  g_object_unref(priv->csock);
  priv->csock = NULL;
  return FALSE;
}


static gboolean referential_application_accept(GSocket *lsock G_GNUC_UNUSED,
						 GIOCondition cond G_GNUC_UNUSED,
						 gpointer data)
{
  ReferentialApplication *app = data;
  ReferentialApplicationPrivate *priv = app->priv;
  GSource *src;

  priv->csock = g_socket_accept(priv->lsock, NULL, NULL);
  if (!priv->csock)
    return FALSE;
  g_object_unref(priv->lsock);
  priv->lsock = NULL;

  src = g_socket_create_source(priv->csock, G_IO_IN, NULL);
  g_source_attach(src, NULL);
  g_object_ref(app);
  g_source_set_callback(src,
			(GSourceFunc)referential_application_read,
			app,
			g_object_unref);


  return FALSE;
}

static gboolean referential_application_listen(ReferentialApplication *app,
						 GError **err)
{
  ReferentialApplicationPrivate *priv = app->priv;
  gchar *path;
  GSocketAddress *addr;
  GSource *src;

  path = g_strdup_printf("%s/.referential", g_get_home_dir());
  unlink(path);

  addr = g_unix_socket_address_new(path);

  priv->lsock = g_socket_new(G_SOCKET_FAMILY_UNIX,
			     G_SOCKET_TYPE_STREAM,
			     G_SOCKET_PROTOCOL_DEFAULT,
			     err);
  if (!priv->lsock)
    return FALSE;

  if (!g_socket_bind(priv->lsock, addr, FALSE, err)) {
    g_object_unref(priv->lsock);
    priv->lsock = NULL;
    return FALSE;
  }
  if (!g_socket_listen(priv->lsock, err)) {
    g_object_unref(priv->lsock);
    priv->lsock = NULL;
    return FALSE;
  }
  g_socket_set_blocking(priv->lsock, FALSE);

  src = g_socket_create_source(priv->lsock, G_IO_IN, NULL);
  g_source_attach(src, NULL);
  g_object_ref(app);
  g_source_set_callback(src,
			(GSourceFunc)referential_application_accept,
			app,
			g_object_unref);


  return TRUE;
}

gboolean referential_application_run(ReferentialApplication *app,
				       GError **err)
{
  ReferentialApplicationPrivate *priv = app->priv;
  gboolean ret;
  gchar *preload;

  if (!referential_application_listen(app, err))
    return FALSE;

  if (priv->filter)
    setenv("REFERENTIAL_FILTER", priv->filter, 1);

  preload = g_strdup_printf("libreferential-%s.so", priv->parasite);

  setenv("LD_PRELOAD", preload, 1);

  ret = g_spawn_command_line_async(priv->command, err);

  unsetenv("LD_PRELOAD");
  unsetenv("REFERENTIAL_FILTER");
  g_free(preload);

  return ret;
}

const gchar *referential_application_get_command(ReferentialApplication *app)
{
  ReferentialApplicationPrivate *priv = app->priv;

  return priv->command;
}


const gchar *referential_application_get_parasite(ReferentialApplication *app)
{
  ReferentialApplicationPrivate *priv = app->priv;

  return priv->parasite;
}


const gchar *referential_application_get_filter(ReferentialApplication *app)
{
  ReferentialApplicationPrivate *priv = app->priv;

  return priv->filter;
}
