/*
 * referential-session.h: gobject watch session
 *
 * Copyright (C) 2011 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * Author: Daniel P. Berrange <berrange@redhat.com>
 */

#ifndef __REFERENTIAL_SESSION_H__
#define __REFERENTIAL_SESSION_H__

#include <glib-object.h>

#include "referential/referential-instance.h"
#include "referential/referential-event.h"
#include "referential/referential-backtrace.h"
#include "referential/referential-symbol.h"

G_BEGIN_DECLS

#define REFERENTIAL_TYPE_SESSION            (referential_session_get_type ())
#define REFERENTIAL_SESSION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), REFERENTIAL_TYPE_SESSION, ReferentialSession))
#define REFERENTIAL_SESSION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), REFERENTIAL_TYPE_SESSION, ReferentialSessionClass))
#define REFERENTIAL_IS_SESSION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), REFERENTIAL_TYPE_SESSION))
#define REFERENTIAL_IS_SESSION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), REFERENTIAL_TYPE_SESSION))
#define REFERENTIAL_SESSION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), REFERENTIAL_TYPE_SESSION, ReferentialSessionClass))


typedef struct _ReferentialSession ReferentialSession;
typedef struct _ReferentialSessionPrivate ReferentialSessionPrivate;
typedef struct _ReferentialSessionClass ReferentialSessionClass;

struct _ReferentialSession
{
    GObject parent;

    ReferentialSessionPrivate *priv;

    /* Do not add fields to this struct */
};

struct _ReferentialSessionClass
{
    GObjectClass parent_class;

    gpointer padding[20];
};

GType referential_session_get_type(void);

ReferentialSession *referential_session_new(void);

ReferentialSession *referential_session_snapshot(ReferentialSession *session);

void referential_session_add_instance(ReferentialSession *session,
					ReferentialInstance *inst);
void referential_session_add_backtrace(ReferentialSession *session,
					 ReferentialBacktrace *backtrace);
void referential_session_add_symbol(ReferentialSession *session,
				      ReferentialSymbol *syminfo);

ReferentialInstance *referential_session_find_instance(ReferentialSession *session,
							  gpointer address);

ReferentialBacktrace *referential_session_find_backtrace(ReferentialSession *session,
							    guint64 id);

ReferentialSymbol *referential_session_find_symbol(ReferentialSession *session,
						      gpointer address);

GList *referential_session_get_instances(ReferentialSession *session);


G_END_DECLS

#endif /* __REFERENTIAL_SESSION_H__ */
