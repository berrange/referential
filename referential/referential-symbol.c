/*
 * referential-symbol.c: gobject watch symbol
 *
 * Copyright (C) 2011 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * Author: Daniel P. Berrange <berrange@redhat.com>
 */

#define _GNU_SOURCE
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>

#include "referential/referential-symbol.h"

#define REFERENTIAL_SYMBOL_GET_PRIVATE(obj)                         \
        (G_TYPE_INSTANCE_GET_PRIVATE((obj), REFERENTIAL_TYPE_SYMBOL, ReferentialSymbolPrivate))

struct _ReferentialSymbolPrivate
{
  gpointer address;
  gchar *loadpath;
  gchar *loadfile;
  gpointer loadaddr;
  gchar *symname;
  gpointer symaddr;
  gchar *srcpath;

  gboolean resolved;
};

G_DEFINE_TYPE(ReferentialSymbol, referential_symbol, G_TYPE_OBJECT);

enum {
    PROP_0,
    PROP_ADDRESS,
    PROP_LOADPATH,
    PROP_LOADFILE,
    PROP_LOADADDR,
    PROP_SYMNAME,
    PROP_SYMADDR,
    PROP_SRCPATH,
};


static void referential_symbol_get_property(GObject *object,
					      guint prop_id,
					      GValue *value,
					      GParamSpec *pspec)
{
    ReferentialSymbol *symbol = REFERENTIAL_SYMBOL(object);
    ReferentialSymbolPrivate *priv = symbol->priv;

    switch (prop_id) {
    case PROP_ADDRESS:
        g_value_set_pointer(value, priv->address);
        break;

    case PROP_LOADPATH:
        g_value_set_string(value, priv->loadpath);
        break;

    case PROP_LOADFILE:
      g_value_set_string(value, priv->loadfile);
        break;

    case PROP_LOADADDR:
        g_value_set_pointer(value, priv->loadaddr);
        break;

    case PROP_SYMNAME:
        g_value_set_string(value, priv->symname);
        break;

    case PROP_SYMADDR:
        g_value_set_pointer(value, priv->symaddr);
        break;

    case PROP_SRCPATH:
        g_value_set_string(value, priv->srcpath);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void referential_symbol_set_property(GObject *object,
					      guint prop_id,
					      const GValue *value,
					      GParamSpec *pspec)
{
    ReferentialSymbol *symbol = REFERENTIAL_SYMBOL(object);
    ReferentialSymbolPrivate *priv = symbol->priv;
    gchar *tmp;

    switch (prop_id) {
    case PROP_ADDRESS:
        priv->address = g_value_get_pointer(value);
        break;

    case PROP_LOADPATH:
        priv->loadpath = g_value_dup_string(value);
	tmp = strrchr(priv->loadpath, '/');
	if (tmp)
	  priv->loadfile = g_strdup(tmp+1);
	else
	  priv->loadfile = g_strdup(priv->loadpath);
        break;

    case PROP_LOADADDR:
        priv->loadaddr = g_value_get_pointer(value);
        break;

    case PROP_SYMNAME:
        priv->symname = g_value_dup_string(value);
        break;

    case PROP_SYMADDR:
        priv->symaddr = g_value_get_pointer(value);
        break;

    case PROP_SRCPATH:
        priv->srcpath = g_value_dup_string(value);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void referential_symbol_finalize(GObject *object)
{
    ReferentialSymbol *symbol = REFERENTIAL_SYMBOL(object);
    ReferentialSymbolPrivate *priv = symbol->priv;

    g_free(priv->loadpath);
    g_free(priv->loadfile);
    g_free(priv->symname);
    g_free(priv->srcpath);

    G_OBJECT_CLASS(referential_symbol_parent_class)->finalize(object);
}


static void referential_symbol_class_init(ReferentialSymbolClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = referential_symbol_finalize;
    object_class->get_property = referential_symbol_get_property;
    object_class->set_property = referential_symbol_set_property;

    g_object_class_install_property(object_class,
                                    PROP_ADDRESS,
                                    g_param_spec_pointer("address",
							 "Address",
							 "Symbol address",
							 G_PARAM_READABLE |
							 G_PARAM_WRITABLE |
							 G_PARAM_CONSTRUCT_ONLY |
							 G_PARAM_STATIC_NAME |
							 G_PARAM_STATIC_NICK |
							 G_PARAM_STATIC_BLURB));
    g_object_class_install_property(object_class,
                                    PROP_LOADPATH,
                                    g_param_spec_string("loadpath",
							"Load path",
							"The binary load path",
							NULL,
							G_PARAM_READABLE |
							G_PARAM_WRITABLE |
							G_PARAM_CONSTRUCT_ONLY |
							G_PARAM_STATIC_NAME |
							G_PARAM_STATIC_NICK |
							G_PARAM_STATIC_BLURB));
    g_object_class_install_property(object_class,
                                    PROP_LOADFILE,
                                    g_param_spec_string("loadfile",
							"Load file",
							"The binary load file",
							NULL,
							G_PARAM_READABLE |
							G_PARAM_STATIC_NAME |
							G_PARAM_STATIC_NICK |
							G_PARAM_STATIC_BLURB));
    g_object_class_install_property(object_class,
                                    PROP_LOADADDR,
                                    g_param_spec_pointer("loadaddr",
							 "Load address",
							 "The binary load address",
							 G_PARAM_READABLE |
							 G_PARAM_WRITABLE |
							 G_PARAM_CONSTRUCT_ONLY |
							 G_PARAM_STATIC_NAME |
							 G_PARAM_STATIC_NICK |
							 G_PARAM_STATIC_BLURB));
    g_object_class_install_property(object_class,
                                    PROP_SYMNAME,
                                    g_param_spec_string("symname",
							"Symbol name",
							"The symbol name",
							NULL,
							G_PARAM_READABLE |
							G_PARAM_WRITABLE |
							G_PARAM_CONSTRUCT_ONLY |
							G_PARAM_STATIC_NAME |
							G_PARAM_STATIC_NICK |
							G_PARAM_STATIC_BLURB));
    g_object_class_install_property(object_class,
                                    PROP_SYMADDR,
                                    g_param_spec_pointer("symaddr",
							 "Symbol address",
							 "The symbol address",
							 G_PARAM_READABLE |
							 G_PARAM_WRITABLE |
							 G_PARAM_CONSTRUCT_ONLY |
							 G_PARAM_STATIC_NAME |
							 G_PARAM_STATIC_NICK |
							 G_PARAM_STATIC_BLURB));
    g_object_class_install_property(object_class,
                                    PROP_SRCPATH,
                                    g_param_spec_string("srcpath",
							"Source path",
							"The source file path",
							NULL,
							G_PARAM_READABLE |
							G_PARAM_WRITABLE |
							G_PARAM_CONSTRUCT_ONLY |
							G_PARAM_STATIC_NAME |
							G_PARAM_STATIC_NICK |
							G_PARAM_STATIC_BLURB));

    g_type_class_add_private(klass, sizeof(ReferentialSymbolPrivate));
}


static void referential_symbol_init(ReferentialSymbol *conn)
{
    ReferentialSymbolPrivate *priv;

    priv = conn->priv = REFERENTIAL_SYMBOL_GET_PRIVATE(conn);

    memset(priv, 0, sizeof(*priv));
}


ReferentialSymbol *referential_symbol_new(gpointer address,
					     const gchar *loadpath,
					     gpointer loadaddr,
					     const gchar *symname,
					     gpointer symaddr)
{
    return REFERENTIAL_SYMBOL(g_object_new(REFERENTIAL_TYPE_SYMBOL,
					     "address", address,
					     "loadpath", loadpath,
					     "loadaddr", loadaddr,
					     "symname", symname,
					     "symaddr", symaddr,
					     NULL));
}


gpointer referential_symbol_get_address(ReferentialSymbol *symbol)
{
    ReferentialSymbolPrivate *priv = symbol->priv;

    return priv->address;
}


const gchar *referential_symbol_get_loadpath(ReferentialSymbol *symbol)
{
    ReferentialSymbolPrivate *priv = symbol->priv;

    return priv->loadpath;
}


const gchar *referential_symbol_get_loadfile(ReferentialSymbol *symbol)
{
    ReferentialSymbolPrivate *priv = symbol->priv;

    return priv->loadfile;
}


gpointer referential_symbol_get_loadaddr(ReferentialSymbol *symbol)
{
    ReferentialSymbolPrivate *priv = symbol->priv;

    return priv->loadaddr;
}


const gchar *referential_symbol_get_symname(ReferentialSymbol *symbol)
{
    ReferentialSymbolPrivate *priv = symbol->priv;

    return priv->symname;
}


gpointer referential_symbol_get_symaddr(ReferentialSymbol *symbol)
{
    ReferentialSymbolPrivate *priv = symbol->priv;

    return priv->symaddr;
}

const gchar *referential_symbol_get_srcpath(ReferentialSymbol *symbol)
{
    ReferentialSymbolPrivate *priv = symbol->priv;

    return priv->srcpath ? priv->srcpath : priv->loadpath;
}

void referential_symbol_resolve(ReferentialSymbol *symbol)
{
    ReferentialSymbolPrivate *priv = symbol->priv;

    if (priv->resolved)
      return;

    gchar *debuglib;
    gpointer addr;

    if (strncmp(priv->loadpath, "./", 2) == 0) {
      /* Relative path, resolve from CWD */
      char *cwd = get_current_dir_name();
      debuglib = g_strdup_printf("%s%s", cwd, priv->loadpath + 1);
      g_free(cwd);
    } else if (strchr(priv->loadpath, '/')) {
      /* Absolute path, just canonicalize it */
      char *lib = canonicalize_file_name(priv->loadpath);
      debuglib = g_strdup_printf("/usr/lib/debug%s.debug", lib ? lib : priv->loadpath);
      g_free(lib);
    } else {
      /* No path, must be in $PATH */
      /* XXX we find the binary in $PATH rather than hardcode /usr/bin */
      debuglib = g_strdup_printf("/usr/lib/debug/usr/bin/%s.debug", priv->loadpath);
    }

    /* Libraries need the offset, but the main binary does not */
    if (strstr(priv->loadpath, "lib") && !strstr(priv->loadpath, "bin"))
      addr = (gpointer)((unsigned long long)priv->address -
			(unsigned long long)priv->loadaddr);
    else
      addr = priv->address;

    if (access(debuglib, R_OK) >= 0) {
      char buf[100];
      sprintf(buf, "%p", addr);
      const gchar *addr2line[]= {
	"/usr/bin/addr2line",
	"-s",
	"-f",
	"--exe",
	debuglib,
	buf,
	NULL
      };

      gchar *out = NULL;
      gint status;
      g_spawn_sync(NULL, (gchar **)addr2line, NULL, G_SPAWN_STDERR_TO_DEV_NULL,
		   NULL, NULL, &out, NULL, &status, NULL);

      if (status == 0) {
	gchar *sym = out;
	if (sym) {
	  gchar *src = strchr(out, '\n');
	  if (src) {
	    *src = '\0';
	    src++;
	    gchar *tmp = strchr(src, '\n');
	    if (tmp)
	      *tmp = '\0';
	    if (strcmp(src, "??:0") != 0)
	      priv->srcpath = g_strdup(src);
	  }

	  if (strcmp(sym, "??") != 0) {
	    g_free(priv->symname);
	    priv->symname = sym;
	  }
	}
      } else {
	g_free(out);
      }
    }


    g_free(debuglib);

    priv->resolved = TRUE;
}
