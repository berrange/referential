/*
 * referential-event.h: gobject watch event
 *
 * Copyright (C) 2011 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * Author: Daniel P. Berrange <berrange@redhat.com>
 */

#ifndef __REFERENTIAL_EVENT_H__
#define __REFERENTIAL_EVENT_H__

#include <glib-object.h>

#include "referential/referential-backtrace.h"

G_BEGIN_DECLS

#define REFERENTIAL_TYPE_EVENT            (referential_event_get_type ())
#define REFERENTIAL_EVENT(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), REFERENTIAL_TYPE_EVENT, ReferentialEvent))
#define REFERENTIAL_EVENT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), REFERENTIAL_TYPE_EVENT, ReferentialEventClass))
#define REFERENTIAL_IS_EVENT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), REFERENTIAL_TYPE_EVENT))
#define REFERENTIAL_IS_EVENT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), REFERENTIAL_TYPE_EVENT))
#define REFERENTIAL_EVENT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), REFERENTIAL_TYPE_EVENT, ReferentialEventClass))


typedef struct _ReferentialEvent ReferentialEvent;
typedef struct _ReferentialEventPrivate ReferentialEventPrivate;
typedef struct _ReferentialEventClass ReferentialEventClass;

struct _ReferentialEvent
{
    GObject parent;

    ReferentialEventPrivate *priv;

    /* Do not add fields to this struct */
};

struct _ReferentialEventClass
{
    GObjectClass parent_class;

    gpointer padding[20];
};

GType referential_event_get_type(void);

ReferentialEvent *referential_event_new(int evtype,
					   guint refs,
					   ReferentialBacktrace *backtrace);

int referential_event_get_evtype(ReferentialEvent *event);
guint referential_event_get_refs(ReferentialEvent *event);
ReferentialBacktrace *referential_event_get_backtrace(ReferentialEvent *event);

G_END_DECLS

#endif /* __REFERENTIAL_EVENT_H__ */
