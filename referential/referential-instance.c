/*
 * referential-instance.c: gobject watch instance
 *
 * Copyright (C) 2011 Daniel P. Berrange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * Author: Daniel P. Berrange <berrange@redhat.com>
 */

#include <string.h>

#include "referential/referential-instance.h"

#define REFERENTIAL_INSTANCE_GET_PRIVATE(obj)                         \
        (G_TYPE_INSTANCE_GET_PRIVATE((obj), REFERENTIAL_TYPE_INSTANCE, ReferentialInstancePrivate))

struct _ReferentialInstancePrivate
{
  gpointer address;
  guint64 id;
  gchar *klass;
  GList *events;
};

G_DEFINE_TYPE(ReferentialInstance, referential_instance, G_TYPE_OBJECT);

enum {
    PROP_0,
    PROP_ADDRESS,
    PROP_ID,
    PROP_KLASS,
};


static void referential_instance_get_property(GObject *object,
						guint prop_id,
						GValue *value,
						GParamSpec *pspec)
{
    ReferentialInstance *instance = REFERENTIAL_INSTANCE(object);
    ReferentialInstancePrivate *priv = instance->priv;

    switch (prop_id) {
    case PROP_ADDRESS:
        g_value_set_pointer(value, priv->address);
        break;

    case PROP_ID:
        g_value_set_uint64(value, priv->id);
        break;

    case PROP_KLASS:
        g_value_set_string(value, priv->klass);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void referential_instance_set_property(GObject *object,
						guint prop_id,
						const GValue *value,
						GParamSpec *pspec)
{
    ReferentialInstance *instance = REFERENTIAL_INSTANCE(object);
    ReferentialInstancePrivate *priv = instance->priv;

    switch (prop_id) {
    case PROP_ADDRESS:
        priv->address = g_value_get_pointer(value);
        break;

    case PROP_ID:
        priv->id = g_value_get_uint64(value);
        break;

    case PROP_KLASS:
        priv->klass = g_value_dup_string(value);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
    }
}


static void referential_instance_finalize(GObject *object)
{
    ReferentialInstance *instance = REFERENTIAL_INSTANCE(object);
    ReferentialInstancePrivate *priv = instance->priv;

    g_free(priv->klass);

    g_list_foreach(priv->events, (GFunc)g_object_unref, NULL);
    g_list_free(priv->events);

    G_OBJECT_CLASS(referential_instance_parent_class)->finalize(object);
}


static void referential_instance_class_init(ReferentialInstanceClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = referential_instance_finalize;
    object_class->get_property = referential_instance_get_property;
    object_class->set_property = referential_instance_set_property;

    g_object_class_install_property(object_class,
                                    PROP_ADDRESS,
                                    g_param_spec_pointer("address",
							 "Address",
							 "The object address",
							 G_PARAM_READABLE |
							 G_PARAM_WRITABLE |
							 G_PARAM_CONSTRUCT_ONLY |
							 G_PARAM_STATIC_NAME |
							 G_PARAM_STATIC_NICK |
							 G_PARAM_STATIC_BLURB));
    g_object_class_install_property(object_class,
                                    PROP_ID,
                                    g_param_spec_uint64("id",
							"ID",
							"The object ID",
							0,
							G_MAXUINT64,
							0,
							G_PARAM_READABLE |
							G_PARAM_WRITABLE |
							G_PARAM_CONSTRUCT_ONLY |
							G_PARAM_STATIC_NAME |
							G_PARAM_STATIC_NICK |
							G_PARAM_STATIC_BLURB));
    g_object_class_install_property(object_class,
                                    PROP_KLASS,
                                    g_param_spec_string("klass",
							"Klass",
							"The klass name",
							NULL,
							G_PARAM_READABLE |
							G_PARAM_WRITABLE |
							G_PARAM_CONSTRUCT_ONLY |
							G_PARAM_STATIC_NAME |
							G_PARAM_STATIC_NICK |
							G_PARAM_STATIC_BLURB));

    g_type_class_add_private(klass, sizeof(ReferentialInstancePrivate));
}


static void referential_instance_init(ReferentialInstance *conn)
{
    ReferentialInstancePrivate *priv;

    priv = conn->priv = REFERENTIAL_INSTANCE_GET_PRIVATE(conn);

    memset(priv, 0, sizeof(*priv));
}


ReferentialInstance *referential_instance_new(gpointer address,
					      guint64 id,
					      const gchar *klass)
{
    return REFERENTIAL_INSTANCE(g_object_new(REFERENTIAL_TYPE_INSTANCE,
					     "address", address,
					     "id", id,
					     "klass", klass,
					     NULL));
}

gpointer referential_instance_get_address(ReferentialInstance *instance)
{
    ReferentialInstancePrivate *priv = instance->priv;

    return priv->address;
}

guint64 referential_instance_get_id(ReferentialInstance *instance)
{
    ReferentialInstancePrivate *priv = instance->priv;

    return priv->id;
}

const gchar *referential_instance_get_klass(ReferentialInstance *instance)
{
    ReferentialInstancePrivate *priv = instance->priv;

    return priv->klass;
}

guint referential_instance_get_refs(ReferentialInstance *instance)
{
    ReferentialInstancePrivate *priv = instance->priv;
    GList *tmp = priv->events;

    if (!tmp)
      return 0;
    
    while (tmp && tmp->next)
      tmp = tmp->next;
    return referential_event_get_refs(tmp->data);
}


void referential_instance_add_event(ReferentialInstance *instance,
				      ReferentialEvent *event)
{
    ReferentialInstancePrivate *priv = instance->priv;

    g_object_ref(event);
    priv->events = g_list_append(priv->events, event);
}

GList *referential_instance_get_events(ReferentialInstance *instance)
{
    ReferentialInstancePrivate *priv = instance->priv;
    
    return g_list_copy(priv->events);
}
